import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/*
 * Reference:
 * http://docs.oracle.com/javase/tutorial/jaxp/sax/parsing.html 
 */
public class SAXParser extends DefaultHandler {
	
	private int count = 0;
	private String param = "";
	private String value = "";
	public List<String> nodeValues = null;
	boolean assignmentNode = false;
	char vals[] = new char[100];
	int i=0;
	
 	public int getCount() {
		return count;
	}
 	
 	public SAXParser setValue(String value) {
		this.value = value;
		return this;
	}
 	
 	public SAXParser setParam(String param) {
		this.param = param;
		return this;
	}
	public void startElement(String namespaceURI,
            String localName,
            String qName, 
            Attributes atts) throws SAXException {
		if (qName.equalsIgnoreCase(param)) {
			assignmentNode = true;
		}
	}
	
	public void endElement(String namespaceURI,
            String localName,
            String qName) throws SAXException {
		if (qName.equalsIgnoreCase(param)) {
			assignmentNode = false;
			vals[i] = '\0';
			i=0;
			String assignmentName = new String(vals);
			if (assignmentName.trim().equalsIgnoreCase(value.trim())) {
				count++;
			}
			vals = new char[100];
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		if (assignmentNode) {
			for(int j=start; j<start+length; j++, i++) {
				vals[i] = ch[j];
			}
		}
	}
	
	public void startDocument() throws SAXException {
		nodeValues = new ArrayList<String>();
    }
	
	public void endDocument() throws SAXException {
    }
}