import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class TrafficCameraReportServlet extends HttpServlet {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private static final String INPUT_URL = "http://www.cs.utexas.edu/~devdatta/traffic_cameras_data.xml";
	Map<String, Integer> localCache;
	
	@Override
    public void init() throws ServletException {
		localCache = new HashMap<>();		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if (request.getHeader("If-Match") != null) {
			String eTag = request.getHeader("If-Match");
			Integer value = localCache.get(eTag);
			if (value != null) {
				response.addHeader("Content-Type", "text/plain");
				response.addHeader("Etag", eTag);
				response.getWriter().println(value);
				return;
			}
		}
		
		String[] paramAndValue = getParamAndValue(request);
		if (paramAndValue[0] == null || paramAndValue[1] == null) {
			response.getWriter().println("no query parameter found");
			return;
		}
		XMLReader xmlReader = buildXmlReader(paramAndValue[0], paramAndValue[1]);
		try {
			xmlReader.parse(new InputSource(new URL(INPUT_URL).openStream()));
		} catch (SAXException e) {
			e.printStackTrace();
			throw new IllegalStateException("Parser failed");
		}
		int count = ((SAXParser) xmlReader.getContentHandler()).getCount();
		String val = request.getQueryString().substring(request.getQueryString().indexOf('=') + 1).trim();
		String eTag = paramAndValue[0] + "=" + val;
		localCache.put(eTag, count);
		response.addHeader("Content-Type", "text/plain");
		response.addHeader("Etag", eTag);
		response.getWriter().println("Number of cameras with " + paramAndValue[0] +  " '" + val + "': " + count);
	}
	
	// Returns XML reader.
	private XMLReader buildXmlReader(String param, String value) {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		javax.xml.parsers.SAXParser saxParser;
		try {
			saxParser = spf.newSAXParser();
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
			throw new IllegalStateException("getting parser from factory failed");
		}
		XMLReader xmlReader;
		try {
			xmlReader = saxParser.getXMLReader();
		} catch (SAXException e) {
			e.printStackTrace();
			throw new IllegalStateException("getting XML Reader from parser failed");
		}
		
		SAXParser mySaxParser = new SAXParser();
		xmlReader.setContentHandler(mySaxParser
				.setParam("ds:" + param)
				.setValue(value));
		return xmlReader;
	}
	
	// Returns an array with arr[0] the parameter, arr[1] the value.
	private String[] getParamAndValue(HttpServletRequest request) {
		String[] paramAndValue = new String[2];
		String val = request.getParameter("camera_status");
		if (val != null) {
			paramAndValue[0] = "camera_status";
			paramAndValue[1] = val;
			return paramAndValue;
		}
		val = request.getParameter("camera_mfg");
		if (val != null) {
			paramAndValue[0] = "camera_mfg";
			paramAndValue[1] = val;
			return paramAndValue;
		}
		val = request.getParameter("location_type");
		if (val != null) {	
			paramAndValue[0] = "location_type";
			paramAndValue[1] = val;
			return paramAndValue;
		}
		val = request.getParameter("ip_comm_status");
		if (val != null) {
			paramAndValue[0] = "ip_comm_status";
			paramAndValue[1] = val;
			return paramAndValue;
		}
		return paramAndValue;
	}
}
